package mainModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * This is mail class used to declaring variables, webDriver
 * @author Hardik Soni
 *
 */
public class WebDriverInitialization {

	public WebDriver driver = null;

	public String homePageURL = "http://ec2-54-174-213-136.compute-1.amazonaws.com:8080/admin";

	/**
	 * No-arg constructor
	 */
	public void InitializeWebDriver() {
		
		System.setProperty("webdriver.chrome.driver", "chromedriver");
		// Uncomment below code to run in Windows machine and comment above line.
		//	System.setProperty("webdriver.chrome.driver", "chromedriver.exec");
		driver = new ChromeDriver();
	}
}