package locators.headerlocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This is locator class for the Header section for the Page
 * @author Hardik Soni
 *
 */
public class HeaderLocators {

	private String UserHeader = "//div[@class='header']//li[@id='users']";
	
	/**
	 * This method will give WebElement for the 'User' button in Header section
	 * @param driver
	 * @return WebElement
	 */
	public WebElement getUserHeaderButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(UserHeader)));

		return driver.findElement(By.xpath(UserHeader));
	}
}
