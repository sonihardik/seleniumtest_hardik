package locators.newuserlocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This is locator class used for User screen.
 * 
 * @author Hardik Soni
 *
 */
public class UserLocators {

	private String newUserButton = "//div[@id='titlebar_right']//a[@href='/admin/users/new']";
	private String userTitle = "page_title";

	private String filterTitle = "//div[@id='filters_sidebar_section']/h3";

	private String filterUserNameTitle = "//div[@id='filters_sidebar_section']//label[@for='q_username']";
	private String filterUserNameKey = "//div[@id='filters_sidebar_section']//div[@id='q_username_input']/select";
	private String filterUserNameValue = "//div[@id='filters_sidebar_section']//input[@id='q_username']";

	private String filterEmailTitle = "//div[@id='filters_sidebar_section']//label[@for='q_email']";
	private String filterEmailKey = "//div[@id='filters_sidebar_section']//div[@id='q_email_input']/select";
	private String filterEmailValue = "//div[@id='filters_sidebar_section']//input[@id='q_email']";

	private String filterCreatedAtTitle = "//div[@id='filters_sidebar_section']//div[@id='q_created_at_input']//label[@class='label']";
	private String filterFromDate = "//div[@id='filters_sidebar_section']//div[@id='q_created_at_input']//input[@id='q_created_at_gteq_datetime']";
	private String filterToDate = "//div[@id='filters_sidebar_section']//div[@id='q_created_at_input']//input[@id='q_created_at_lteq_datetime']";
	private String filterDateCalender = "//div[@id='ui-datepicker-div']//table[@class='ui-datepicker-calendar']/tbody";

	private String filterSubmitButton = "//div[@id='filters_sidebar_section']//input[@type='submit']";
	private String filterClearButton = "//div[@id='filters_sidebar_section']//a[@class='clear_filters_btn']";

	private String filterResultsTable = "//div[@id='active_admin_content']//form[@id='collection_selection']//div[@class='index_as_table']/table/tbody";
	private String filterNoResultsTable = "//div[@id='active_admin_content']//form[@id='collection_selection']//div[@class='blank_slate_container']/span";

	/**
	 * This method will return WebElement for the error message after no user found
	 * into DB
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterNoResultsTable(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterNoResultsTable)));

		return driver.findElement(By.xpath(filterNoResultsTable));
	}

	/**
	 * This method will return WebElement for the Result table after successful
	 * found of User entry into DB
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterResultsTable(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterResultsTable)));

		return driver.findElement(By.xpath(filterResultsTable));
	}

	// ------------------------------------------------------------Filter
	// Section------------------------------------------------------------

	/**
	 * This method will return WebElement for the Filter Sections's Date Calender's
	 * pop up displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterDateCalender(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterDateCalender)));

		return driver.findElement(By.xpath(filterDateCalender));
	}

	/**
	 * This method will return WebElement for the Filter Sections's CLEARN filter
	 * button displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterClearButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterClearButton)));

		return driver.findElement(By.xpath(filterClearButton));
	}

	/**
	 * This method will return WebElement for the Filter Sections's SUBMIT button
	 * displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterSubmitButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterSubmitButton)));

		return driver.findElement(By.xpath(filterSubmitButton));
	}

	/**
	 * This method will return WebElement for the Filter Sections's CreatedAT's
	 * toDate displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterToDate(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterToDate)));

		return driver.findElement(By.xpath(filterToDate));
	}

	/**
	 * This method will return WebElement for the Filter Sections's CreatedAT's
	 * fromDate displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterFromDate(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterFromDate)));

		return driver.findElement(By.xpath(filterFromDate));
	}

	/**
	 * This method will return WebElement for the Filter Sections's CreatedAt Title
	 * displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterCreatedAtTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterCreatedAtTitle)));

		return driver.findElement(By.xpath(filterCreatedAtTitle));
	}

	/**
	 * This method will return WebElement for the Filter Sections's Email value's
	 * editBox displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterEmailValue(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterEmailValue)));

		return driver.findElement(By.xpath(filterEmailValue));
	}

	/**
	 * This method will return WebElement for the Filter Sections's Email key/drop
	 * down value displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterEmailKey(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterEmailKey)));

		return driver.findElement(By.xpath(filterEmailKey));
	}

	/**
	 * This method will return WebElement for the Filter Sections's Email Title
	 * displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterEmailTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterEmailTitle)));

		return driver.findElement(By.xpath(filterEmailTitle));
	}

	/**
	 * This method will return WebElement for the Filter Sections's UserName's value
	 * in EditBox displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterUserNameValue(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterUserNameValue)));

		return driver.findElement(By.xpath(filterUserNameValue));
	}

	/**
	 * This method will return WebElement for the Filter Sections's UserName
	 * Value/drop down box displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterUserNameKey(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterUserNameKey)));

		return driver.findElement(By.xpath(filterUserNameKey));
	}

	/**
	 * This method will return WebElement for the Filter Sections's UserName Title
	 * displayed on left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterUserNameTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterUserNameTitle)));

		return driver.findElement(By.xpath(filterUserNameTitle));
	}

	/**
	 * This method will return WebElement for the Filter Sections Title displayed on
	 * left hand side of the screen
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getFilterTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(filterTitle)));

		return driver.findElement(By.xpath(filterTitle));
	}
	
	// ------------------------------------------------------------Title
		// Section------------------------------------------------------------
	
	/**
	 * This method will return WebElement for the New User button displayed on left
	 * hand side of the page
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getNewUseButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(newUserButton)));

		return driver.findElement(By.xpath(newUserButton));
	}

	/**
	 * This method will return WebElement for the User Title displayed on top of the
	 * page
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getUserTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(userTitle)));

		return driver.findElement(By.id(userTitle));
	}
}
