package locators.newuserlocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This is locator class for the Create New User section for the Page under
 * 'User' section
 * 
 * @author Hardik Soni
 *
 */
public class CreateNewUserLocators {

	private String userNameText = "//label[@for='user_username']";
	private String userNameEditBox = "//input[@id='user_username']";
	private String passwordText = "//label[@for='user_password']";
	private String passwordEditBox = "//input[@id='user_password']";
	private String emailText = "//label[@for='user_email']";
	private String emailEditBox = "//input[@id='user_email']";

	private String createUserButton = "//input[@type='submit' and @name ='commit']";
	private String cancelButton = "//li[@class='cancel]";

	private String userTitle = "page_title";

	/**
	 * This method will give WebElement for the 'UserName' Text displayed in
	 * registration form
	 * 
	 * @param driver
	 * @return WebElement
	 */
	public WebElement getUserNameText(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(userNameText)));

		return driver.findElement(By.xpath(userNameText));
	}

	/**
	 * This method will give WebElement for the Edit Box for 'UserName' field
	 * displayed in registration form
	 * 
	 * @param driver
	 * @return WebElement
	 */
	public WebElement getUserNameEditBox(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(userNameEditBox)));

		return driver.findElement(By.xpath(userNameEditBox));
	}

	/**
	 * This method will give WebElement for the 'Password' Text displayed in
	 * registration form
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getPasswordText(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(passwordText)));

		return driver.findElement(By.xpath(passwordText));
	}

	/**
	 * This method will give WebElement for the Edit Box for 'Password' field
	 * displayed in registration form
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getPasswordEditBox(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(passwordEditBox)));

		return driver.findElement(By.xpath(passwordEditBox));
	}

	/**
	 * This method will give WebElement for the 'Email' Text displayed in
	 * registration form
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getEmailText(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(emailText)));

		return driver.findElement(By.xpath(emailText));
	}

	/**
	 * This method will give WebElement for the Edit Box for 'Email' field displayed
	 * in registration form
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getEmailEditBox(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(emailEditBox)));

		return driver.findElement(By.xpath(emailEditBox));
	}

	/**
	 * This method will give WebElement for the submit button displayed in
	 * registration form
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCreateUserButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(createUserButton)));

		return driver.findElement(By.xpath(createUserButton));
	}

	/**
	 * This method will give WebElement for the cancel button displayed in
	 * registration form
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCancelUserButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(cancelButton)));

		return driver.findElement(By.xpath(cancelButton));
	}

	/**
	 * This method will give WebElement for the Title of the page
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getUserTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(userTitle)));

		return driver.findElement(By.id(userTitle));
	}

}
