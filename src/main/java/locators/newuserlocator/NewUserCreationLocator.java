package locators.newuserlocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This is locator class for the Screen after New User get created.
 * 
 * @author Hardik Soni
 *
 */
public class NewUserCreationLocator {

	private String userTitle = "page_title";
	private String editUser = "//div[@id='titlebar_right']//a[text()='Edit User']";
	private String deleteUser = "//div[@id='titlebar_right']//a[text()='Delete User']";

	private String userMessage = "//div[@class='flash flash_notice']";

	private String orderHistoryTitle = "//div[@id='active_admin_content']//div[@id='main_content']/div/h3";
	private String addressBookTitle = "//div[@id='active_admin_content']//div[@id='main_content']/div[2]/h3";
	private String commentsTitle = "//div[@id='active_admin_content']//div[@id='main_content']/div[3]/h3";

	private String customerDetailsTitle = "//div[@id='customer-details_sidebar_section']/h3";
	private String customerDetailsUserName = "//div[@id='customer-details_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[1]/th";
	private String customerDetailsUserValue = "//div[@id='customer-details_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[1]/td";
	private String customerDetailsEmail = "//div[@id='customer-details_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[2]/th";
	private String customerDetailsEmailValue = "//div[@id='customer-details_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[2]/td";
	private String customerDetailsCreatedAt = "//div[@id='customer-details_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[3]/th";
	private String customerDetailsCreatedAtValue = "//div[@id='customer-details_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[3]/td";

	private String orderHistoryTitleLeftSide = "//div[@id='order-history_sidebar_section']/h3";
	private String orderHistoryTotalOrders = "//div[@id='order-history_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[1]/th";
	private String orderHistoryTotalOrdersValue = "//div[@id='order-history_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[1]/td";
	private String orderHistoryTotalValue = "//div[@id='order-history_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[2]/th";
	private String orderHistoryTotalValueValue = "//div[@id='order-history_sidebar_section']//div[@class='attributes_table user']/table/tbody/tr[2]/td";

	/**
	 * This method will give WebElement for the value location of
	 * 'oderHistoryTotalValue' part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getOrderHistoryTotalValue(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(orderHistoryTotalValue)));

		return driver.findElement(By.xpath(orderHistoryTotalValue));
	}

	/**
	 * This method will give WebElement for the key location of
	 * 'oderHistoryTotalValue' part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getOrderHistoryTotalValueValue(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(orderHistoryTotalValueValue)));

		return driver.findElement(By.xpath(orderHistoryTotalValueValue));
	}

	/**
	 * This method will give WebElement for the key location of
	 * 'OrderHistoryTotalOrder' part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getOrderHistoryTotalOrder(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(orderHistoryTotalOrders)));

		return driver.findElement(By.xpath(orderHistoryTotalOrders));
	}

	/**
	 * This method will give WebElement for the value location of
	 * 'OrderHistoryTotalOrder' part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getOrderHistoryTotalOrderValue(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(orderHistoryTotalOrdersValue)));

		return driver.findElement(By.xpath(orderHistoryTotalOrdersValue));
	}

	/**
	 * This method will give WebElement for the key location of 'OrderHistory' title
	 * part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getOrderHistoryTitleLeftSide(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(orderHistoryTitleLeftSide)));

		return driver.findElement(By.xpath(orderHistoryTitleLeftSide));
	}

	/**
	 * This method will give WebElement for the key location of 'Created At' title
	 * part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCustomerDetailsCreatedAtTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(customerDetailsCreatedAt)));

		return driver.findElement(By.xpath(customerDetailsCreatedAt));
	}

	/**
	 * This method will give WebElement for the value location of 'CreatedAt' part
	 * on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCustomerDetailsCreatedAtValue(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(customerDetailsCreatedAtValue)));

		return driver.findElement(By.xpath(customerDetailsCreatedAtValue));
	}

	/**
	 * This method will give WebElement for the key location of 'Email' title part
	 * on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCustomerDetailsEmailTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(customerDetailsEmail)));

		return driver.findElement(By.xpath(customerDetailsEmail));
	}

	/**
	 * This method will give WebElement for the dropDown box location of 'Email'
	 * part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCustomerDetailsEmailValue(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(customerDetailsEmailValue)));

		return driver.findElement(By.xpath(customerDetailsEmailValue));
	}

	/**
	 * This method will give WebElement for the key location of 'UserName' title
	 * part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCustomerDetailsUserNameTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(customerDetailsUserName)));

		return driver.findElement(By.xpath(customerDetailsUserName));
	}

	/**
	 * This method will give WebElement for the drop down location of 'UserName'
	 * title part on left hand side.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCustomerDetailsUserValueTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(customerDetailsUserValue)));

		return driver.findElement(By.xpath(customerDetailsUserValue));
	}

	/**
	 * This method will give WebElement for the Customer details title which gets
	 * create after new user.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCustomerDetailsTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(customerDetailsTitle)));

		return driver.findElement(By.xpath(customerDetailsTitle));
	}

	/**
	 * This method will give WebElement for the Customer comments title which gets
	 * create after new user.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getCommentsTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(commentsTitle)));

		return driver.findElement(By.xpath(commentsTitle));
	}

	/**
	 * This method will give WebElement for the Customer Address title which gets
	 * create after new user.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getAddressBookTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(addressBookTitle)));

		return driver.findElement(By.xpath(addressBookTitle));
	}

	/**
	 * This method will give WebElement for the Customer Order History title which
	 * gets create after new user.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getOrderHistoryTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(orderHistoryTitle)));

		return driver.findElement(By.xpath(orderHistoryTitle));
	}

	/**
	 * This method will give WebElement for the Customer details Welcome message
	 * which gets create after new user.
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getUserMessageText(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(userMessage)));

		return driver.findElement(By.xpath(userMessage));
	}

	/**
	 * This method will give WebElement for the Delete user button
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getDeleteUserButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(deleteUser)));

		return driver.findElement(By.xpath(deleteUser));
	}

	/**
	 * This method will give WebElement for the Edit user button
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getEditUserButton(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(editUser)));

		return driver.findElement(By.xpath(editUser));
	}

	/**
	 * This method will give WebElement for the User Title i.e. UserName
	 * 
	 * @param driver
	 * @return
	 */
	public WebElement getUserTitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(userTitle)));

		return driver.findElement(By.id(userTitle));
	}
}
