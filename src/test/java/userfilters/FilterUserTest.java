package userfilters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import locators.headerlocators.HeaderLocators;
import locators.newuserlocator.CreateNewUserLocators;
import locators.newuserlocator.NewUserCreationLocator;
import locators.newuserlocator.UserLocators;
import mainModel.WebDriverInitialization;

/**
 * Test class for the FilterUser section.
 * 
 * @author Hardik Soni
 *
 */
public class FilterUserTest extends WebDriverInitialization {
	UserLocators userLocators = new UserLocators();
	HeaderLocators headerLocators = new HeaderLocators();
	CreateNewUserLocators createNewUserLocators = new CreateNewUserLocators();
	NewUserCreationLocator newUserCreationLocator = new NewUserCreationLocator();

	String userName, email;

	/**
	 * Before method to do initial setup before running test cases.
	 */
	@Before
	public void openHomePage() {
		InitializeWebDriver();
		driver.navigate().to(homePageURL);
		headerLocators.getUserHeaderButton(driver).click();
		assertEquals("User Page Title should be Users but coming as " + userLocators.getUserTitle(driver).getText(),
				"Users", userLocators.getUserTitle(driver).getText().trim());
	}

	/**
	 * Close the browser before each execution.
	 */
	@After
	public void closeBrowser() {
		driver.quit();
	}

	/**
	 * This test case will check all the static values like, title placeHolderText
	 * and other things are as expected or not.
	 */
	@Test
	public void testFilterUserPlaceHolderTextAndTitleValuesSuccess() {
		assertEquals(
				"Expected Filter title should be 'Filters' but coming as "
						+ userLocators.getFilterTitle(driver).getText(),
				"Filters", userLocators.getFilterTitle(driver).getText());
		assertEquals(
				"Expected Filter userName title should be 'USERNAME' but coming as "
						+ userLocators.getFilterUserNameTitle(driver).getText(),
				"USERNAME", userLocators.getFilterUserNameTitle(driver).getText());
		assertEquals(
				"Expected Filter email title should be 'Email' but coming as "
						+ userLocators.getFilterEmailTitle(driver).getText(),
				"EMAIL", userLocators.getFilterEmailTitle(driver).getText());
		assertEquals(
				"Expected Filter createdAt title should be 'CREATED AT' but coming as "
						+ userLocators.getFilterCreatedAtTitle(driver).getText(),
				"CREATED AT", userLocators.getFilterCreatedAtTitle(driver).getText());

		assertEquals(
				"Expected Filter createdAt from Date Placeholder should be 'From' but coming as "
						+ userLocators.getFilterFromDate(driver).getAttribute("placeholder"),
				"From", userLocators.getFilterFromDate(driver).getAttribute("placeholder"));

		assertEquals(
				"Expected Filter createdAt To Date Placeholder should be 'To' but coming as "
						+ userLocators.getFilterToDate(driver).getAttribute("placeholder"),
				"To", userLocators.getFilterToDate(driver).getAttribute("placeholder"));

		assertEquals(
				"Expected Filter submit button text should be 'Filter' but coming as "
						+ userLocators.getFilterSubmitButton(driver).getAttribute("value"),
				"Filter", userLocators.getFilterSubmitButton(driver).getAttribute("value"));

		assertEquals(
				"Expected Filter clearfilter button text should be 'Clear Filters' but coming as "
						+ userLocators.getFilterClearButton(driver).getText(),
				"Clear Filters", userLocators.getFilterClearButton(driver).getText());
	}

	/**
	 * This test case will filter user by passing value userName and Email id
	 */
	@Test
	public void testFilterUsingUserNameAndEmailEqualsSuccess() {
		// Create User
		createNewUser();

		// Click on Users again
		headerLocators.getUserHeaderButton(driver).click();

		// Add UserName into user edit box
		Select dropdownUser = new Select(userLocators.getFilterUserNameKey(driver));
		dropdownUser.selectByValue("username_equals");
		userLocators.getFilterUserNameValue(driver).sendKeys(userName);

		// Add email into Email edit box
		Select dropdownEmail = new Select(userLocators.getFilterEmailKey(driver));
		dropdownEmail.selectByValue("email_equals");
		userLocators.getFilterEmailValue(driver).sendKeys(email);

		// Click on Submit button
		userLocators.getFilterSubmitButton(driver).click();

		List<WebElement> rows = userLocators.getFilterResultsTable(driver).findElements(By.tagName("tr"));

		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.tagName("td"));
			for (int i = 0; i < columns.size(); i++) {
				if (i == 0) {
					assertEquals("Style for the 1st Column should be CheckBox but its not", "checkbox",
							columns.get(i).findElement(By.tagName("input")).getAttribute("type"));
				} else if (i == 1) {
					assertNotNull(columns.get(i).findElement(By.tagName("a")).getAttribute("href"));
				} else if (i == 5) {
					List<WebElement> actions = columns.get(i).findElement(By.tagName("div"))
							.findElements(By.tagName("a"));
					ArrayList<String> values = new ArrayList<String>(Arrays.asList("View", "Edit", "Delete"));
					for (int j = 0; j < values.size(); j++) {
						assertEquals("Expected Values for the " + j + "th position was " + values.get(j)
								+ " but coming as " + actions.get(j).getText(), values.get(j),
								actions.get(j).getText());
					}
				} else {
					assertNotNull(columns.get(i).getText());
				}
			}
		}
	}

	/**
	 * This test case will filter user by passing invalid userName.
	 */
	@Test
	public void testFilterUsingInvalidUserNameFailure() {
		// Add UserName into user edit box
		Select dropdownUser = new Select(userLocators.getFilterUserNameKey(driver));
		dropdownUser.selectByValue("username_equals");
		userLocators.getFilterUserNameValue(driver).sendKeys("1333sdvd23r4");

		// Click on Submit button
		userLocators.getFilterSubmitButton(driver).click();
		assertEquals(
				"Expected results when no user found was 'No Users found' but coming as "
						+ userLocators.getFilterNoResultsTable(driver).getText(),
				"No Users found", userLocators.getFilterNoResultsTable(driver).getText());
	}

	/**
	 * This test case will filter user by selecting dates, from and To as
	 * currentDate
	 */
	@Test
	public void testFilterUsingSameFromAndToDate() {
		// Select From Date
		userLocators.getFilterFromDate(driver).click();
		List<WebElement> fromColumns = userLocators.getFilterDateCalender(driver).findElements(By.tagName("td"));

		for (WebElement cell : fromColumns) {
			// Select Today's Date
			if (cell.getText().equals(getCurrentDate())) {
				cell.click();
				break;
			}
		}

		// Select To Date
		userLocators.getFilterToDate(driver).click();
		List<WebElement> toColumns = userLocators.getFilterDateCalender(driver).findElements(By.tagName("td"));
		for (WebElement cell : toColumns) {
			// Select Today's Date
			if (cell.getText().equals(getCurrentDate())) {
				cell.click();
				break;
			}
		}

		// Click on Submit button
		userLocators.getFilterSubmitButton(driver).click();

		List<WebElement> rows = userLocators.getFilterResultsTable(driver).findElements(By.tagName("tr"));

		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.tagName("td"));
			for (int i = 0; i < columns.size(); i++) {
				if (i == 0) {
					assertEquals("Style for the 1st Column should be CheckBox but its not", "checkbox",
							columns.get(i).findElement(By.tagName("input")).getAttribute("type"));
				} else if (i == 1) {
					assertNotNull(columns.get(i).findElement(By.tagName("a")).getAttribute("href"));
				} else if (i == 5) {
					List<WebElement> actions = columns.get(i).findElement(By.tagName("div"))
							.findElements(By.tagName("a"));
					ArrayList<String> values = new ArrayList<String>(Arrays.asList("View", "Edit", "Delete"));
					for (int j = 0; j < values.size(); j++) {
						assertEquals("Expected Values for the " + j + "th position was " + values.get(j)
								+ " but coming as " + actions.get(j).getText(), values.get(j),
								actions.get(j).getText());
					}
				} else {
					assertNotNull(columns.get(i).getText());
				}
			}
		}
	}

	/**
	 * This is reusable function used to create User.
	 */
	public void createNewUser() {
		userLocators.getNewUseButton(driver).click();

		userName = RandomStringUtils.randomAlphabetic(10);
		email = RandomStringUtils.randomAlphabetic(10) + "@april.biz";
		createNewUserLocators.getUserNameEditBox(driver).sendKeys(userName);
		createNewUserLocators.getPasswordEditBox(driver).sendKeys("password");
		createNewUserLocators.getEmailEditBox(driver).sendKeys(email);

		createNewUserLocators.getCreateUserButton(driver).click();

		// Assertion after creating User
		assertEquals(
				"Expected UserName was" + userName + "But coming as "
						+ newUserCreationLocator.getUserTitle(driver).getText(),
				userName, newUserCreationLocator.getUserTitle(driver).getText());

		assertEquals(
				"Expected Welcome message was 'User was successfully created.' but coming as "
						+ newUserCreationLocator.getUserMessageText(driver).getText(),
				"User was successfully created.", newUserCreationLocator.getUserMessageText(driver).getText());
	}

	/**
	 * This function will return Current Date of the system between 1-31
	 * 
	 * @return
	 */
	public String getCurrentDate() {
		LocalDateTime now = LocalDateTime.now();
		return String.valueOf(now.getDayOfMonth());
	}
}
