package createnewuser;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import locators.headerlocators.HeaderLocators;
import locators.newuserlocator.CreateNewUserLocators;
import locators.newuserlocator.NewUserCreationLocator;
import locators.newuserlocator.UserLocators;
import mainModel.WebDriverInitialization;

/**
 * Test class for creating new User
 * 
 * @author Hardik Soni
 *
 */
public class CreateNewUserTest extends WebDriverInitialization {

	HeaderLocators HeaderLocators = new HeaderLocators();
	UserLocators userLocators = new UserLocators();
	CreateNewUserLocators createNewUserLocators = new CreateNewUserLocators();
	NewUserCreationLocator newUserCreationLocator = new NewUserCreationLocator();

	/**
	 * Before method used to setup the required data before running actual test
	 * cases.
	 */
	@Before
	public void openHomePage() {
		InitializeWebDriver();
		driver.navigate().to(homePageURL);
		HeaderLocators.getUserHeaderButton(driver).click();
		assertEquals("User Page Title should be Users but coming as " + userLocators.getUserTitle(driver).getText(),
				"Users", userLocators.getUserTitle(driver).getText().trim());
		userLocators.getNewUseButton(driver).click();
		assertEquals(
				"New User Page Title should be User but coming as "
						+ createNewUserLocators.getUserTitle(driver).getText(),
				"New User", createNewUserLocators.getUserTitle(driver).getText().trim());

	}

	/**
	 * Close the browser after each test case.
	 */
	@After
	public void closeBrowser() {
		driver.quit();
	}

	/**
	 * Create New User by passing all valid data
	 * 
	 * @throws ParseException
	 */
	@Test
	public void createNewUserSuccess() throws ParseException {
		assertEquals(
				"Expected value for User name title should be 'UserName' but coming as "
						+ createNewUserLocators.getUserNameText(driver).getText(),
				"Username*", createNewUserLocators.getUserNameText(driver).getText());
		assertEquals(
				"Expected value for password title should be 'Password' but coming as "
						+ createNewUserLocators.getPasswordText(driver).getText(),
				"Password*", createNewUserLocators.getPasswordText(driver).getText());
		assertEquals(
				"Expected value for email title should be 'Email' but coming as "
						+ createNewUserLocators.getEmailText(driver).getText(),
				"Email", createNewUserLocators.getEmailText(driver).getText());

		String userName = RandomStringUtils.randomAlphabetic(10);
		String email = RandomStringUtils.randomAlphabetic(10) + "@april.biz";
		createNewUserLocators.getUserNameEditBox(driver).sendKeys(userName);
		createNewUserLocators.getPasswordEditBox(driver).sendKeys("password");
		createNewUserLocators.getEmailEditBox(driver).sendKeys(email);

		createNewUserLocators.getCreateUserButton(driver).click();

		// Assertion after creating User
		assertEquals(
				"Expected UserName was" + userName + "But coming as "
						+ newUserCreationLocator.getUserTitle(driver).getText(),
				userName, newUserCreationLocator.getUserTitle(driver).getText());
		assertTrue(newUserCreationLocator.getEditUserButton(driver).isEnabled());
		assertTrue(newUserCreationLocator.getDeleteUserButton(driver).isEnabled());

		assertEquals(
				"Expected Welcome message was 'User was successfully created.' but coming as "
						+ newUserCreationLocator.getUserMessageText(driver).getText(),
				"User was successfully created.", newUserCreationLocator.getUserMessageText(driver).getText());

		// Assert Main Content

		assertEquals(
				"Expected Title for the oder hostory was 'Order History' but coming as "
						+ newUserCreationLocator.getOrderHistoryTitle(driver).getText(),
				"Order History", newUserCreationLocator.getOrderHistoryTitle(driver).getText());

		assertEquals(
				"Expected Title for the address book was 'Address Book' but coming as "
						+ newUserCreationLocator.getAddressBookTitle(driver).getText(),
				"Address Book", newUserCreationLocator.getAddressBookTitle(driver).getText());

		assertEquals(
				"Expected Title for the comments was 'Comments' but coming as "
						+ newUserCreationLocator.getCommentsTitle(driver).getText(),
				"Comments (0)", newUserCreationLocator.getCommentsTitle(driver).getText());

		// Assert Left hand side content
		assertEquals(
				"'Customer Details' was expected at left hadn side after creating user but coming as "
						+ newUserCreationLocator.getCustomerDetailsTitle(driver).getText(),
				"Customer Details", newUserCreationLocator.getCustomerDetailsTitle(driver).getText());

		assertEquals(
				"Expected User Details title was 'USERNAME' but coming as "
						+ newUserCreationLocator.getCustomerDetailsUserNameTitle(driver).getText(),
				"USERNAME", newUserCreationLocator.getCustomerDetailsUserNameTitle(driver).getText());

		assertEquals(
				"Expected User Details value was" + userName + "but coming as "
						+ newUserCreationLocator.getCustomerDetailsUserValueTitle(driver).getText(),
				userName, newUserCreationLocator.getCustomerDetailsUserValueTitle(driver).getText());

		assertEquals(
				"Expected User Email title was 'EMAIL' but coming as "
						+ newUserCreationLocator.getCustomerDetailsEmailTitle(driver).getText(),
				"EMAIL", newUserCreationLocator.getCustomerDetailsEmailTitle(driver).getText());

		assertEquals(
				"Expected User Email value was" + email + "but coming as "
						+ newUserCreationLocator.getCustomerDetailsEmailTitle(driver).getText(),
				email, newUserCreationLocator.getCustomerDetailsEmailValue(driver).getText());

		assertEquals(
				"Expected User CREATED AT value was CREATED AT but coming as "
						+ newUserCreationLocator.getCustomerDetailsCreatedAtTitle(driver).getText(),
				"CREATED AT", newUserCreationLocator.getCustomerDetailsCreatedAtTitle(driver).getText());

		String createdAtValue = newUserCreationLocator.getCustomerDetailsCreatedAtValue(driver).getText();
		Date date = new SimpleDateFormat("MMM d, y HH:mm", Locale.ENGLISH).parse(createdAtValue); // parse input date
		String outputDate = new SimpleDateFormat("yyyy/MM/dd HH:mm").format(date);

		// Get Current Date
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		String currentDate = dtf.format(now);

		assertThat("Expected createdAtDate should be" + currentDate + "but coming as " + outputDate, outputDate,
				containsString(currentDate));

		assertEquals(
				"'Order History' was expected at left hadn side after creating user but coming as "
						+ newUserCreationLocator.getOrderHistoryTitleLeftSide(driver).getText(),
				"Order History", newUserCreationLocator.getOrderHistoryTitleLeftSide(driver).getText());

		assertEquals(
				"'Order History's value 'TOTAL ORDERS' was expected at left hadn side after creating user but coming as "
						+ newUserCreationLocator.getOrderHistoryTotalOrder(driver).getText(),
				"TOTAL ORDERS", newUserCreationLocator.getOrderHistoryTotalOrder(driver).getText());

		assertEquals(
				"'TOTAL ORDERS's value 0 was expected at left hadn side after creating user but coming as "
						+ newUserCreationLocator.getOrderHistoryTotalOrderValue(driver).getText(),
				"0", newUserCreationLocator.getOrderHistoryTotalOrderValue(driver).getText());

		assertEquals(
				"'Order History's 'TOTAL VALUE' was expected at left hadn side after creating user but coming as "
						+ newUserCreationLocator.getOrderHistoryTotalValue(driver).getText(),
				"TOTAL VALUE", newUserCreationLocator.getOrderHistoryTotalValue(driver).getText());

		assertEquals(
				"'Order History's value '$0.00' was expected at left hadn side after creating user but coming as "
						+ newUserCreationLocator.getOrderHistoryTotalValueValue(driver).getText(),
				"$0.00", newUserCreationLocator.getOrderHistoryTotalValueValue(driver).getText());

	}
}
