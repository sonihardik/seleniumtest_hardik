This is selenium based automation project for the site : ec2-54-174-213-136.compute-1.amazonaws.com:8080/admin

To run the test cases into Windows OS do the changes mentioned into WebDriverInitialization.java class

Automated flows:

1. 	Creating New User.
	1.  Happy flow by passing all the valid datas as asserting proper response.
1.  Filter User based on input.
	1. Assert Titles, Values and PlaceHolder text for the Filter feature
	1. Filter User based on UserName and EmailId - Success flow
	1. Filter User by passing invalid UserName.
	1. Filter User by selecting From and To Date.